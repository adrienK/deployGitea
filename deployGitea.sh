#!/bin/bash
# Copyright 2018 Iglou.eu
# Copyright 2018 Adrien Kara
# license that can be found in the LICENSE file.

# CONFIG
aGiteaName="" #Mon super gitea
aGiteaPwd="" #Root password / can be empty

srv_core=""  # 386, amd64, arm-5/6/7/64, mips, mips64, mipsle
srv_mail=""  #toto@tata.titi
srv_name=""  #git.myserver.org
srv_alias="" #gitea.myserver.org code.myserver.com 42.myserver.com
srv_ip="" #Can be empty

# VAR
require="git go nginx curl bash crontab dig"

fnginx="/etc/nginx"

letsKnown="/var/www/letsencrypt"
bDehydrated="/usr/bin/dehydrated"
cDehydrated="/etc/letsencrypt"
uDehydrated="https://raw.githubusercontent.com/lukas2511/dehydrated/master/dehydrated"

giteaUser="gitea"
giteaPort="3000"
bGitea="/usr/bin/gitea"
cGitea="/etc/gitea"
sGitea="/etc/systemd/system/gitea.service"
uGitea="https://dl.gitea.io/gitea/master/gitea-master-linux-${srv_core}"

#linuxDist=$(lsb_release -a | grep ID | awk '{print $NF}')

# FUNCTIONS
function err() {
    echo "FATAL ERROR: ${1}"
    exit 1
}

function return_check()
{
    if [[ $1 -ne 0 ]]; then
        err "${2}"
    fi
}

function dns_check()
{
    add_record=$(dig +short ${srv_name})

    if [[ $add_record != *$srv_ip* ]]; then
            err "L'entrée \"${srv_name} IN A·AAA ${srv_ip}\" n'éxiste pas" \
                "et/ou n'est pas joignable"
    fi
}

function require()
{
    for soft in $require; do
        if [[ -z $(type -p $soft) ]]; then
            err "Merci d'installer ces dépendances \"$require\""
        fi
    done
}

function cronie_base()
{
    systemctl enable crond
    systemctl start crond
    return_check $? "Probléme d'xecution de cronie, voir crond.service"
}

function nginx_base()
{
    if [[ -e "${fnginx}/sites-enabled" ]]; then
        rm ${fnginx}/sites-enabled/*
    else
        mkdir -p "${fnginx}/sites-available"
        mkdir -p "${fnginx}/sites-enabled"

        rm /tmp/nginx.conf

        while read line
        do
            if [[ "http {" = "${line}" ]]; then
                break
            else
                echo "${line}" >> /tmp/nginx.conf
            fi
        done < "${fnginx}/nginx.conf"

        echo "
http {
sendfile on;
tcp_nopush on;
tcp_nodelay on;
keepalive_timeout 65;
types_hash_max_size 2048;
include /etc/nginx/mime.types;
default_type application/octet-stream;
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
gzip on;
gzip_disable \"msie6\";
include ${fnginx}/sites-enabled/*;
}
        " >> /tmp/nginx.conf

        mv "${fnginx}/nginx.conf" "${fnginx}/nginx.conf.back"
        mv "/tmp/nginx.conf" "${fnginx}/nginx.conf"
    fi

    echo "
    server{
        listen 80;
        server_name ${srv_name} ${srv_alias};
	    location /.well-known/acme-challenge {
		    alias ${letsKnown};
	    }
        location ~ ^\/(?!.well-known).* {
                return 301 https://\$host\$request_uri;
        }
    }" > "${fnginx}/sites-available/gitea"

    ln -s "${fnginx}/sites-available/gitea" "${fnginx}/sites-enabled/"

    mkdir -p "${letsKnown}"

    systemctl enable nginx
    systemctl start nginx
    return_check $? "Start NGINX c'est mal passé, voir \"nginx -t\" et son log"
}

function dehydrated_app()
{
    curl "$uDehydrated" -o "${bDehydrated}"
    chmod +x "${bDehydrated}"

    mkdir -p "${cDehydrated}/certs"
    mkdir -p "${cDehydrated}/chains"
    mkdir -p "${cDehydrated}/accounts"

    if [ -n "$srv_alias" ]; then
        echo "${srv_name} ${srv_alias}" > "${cDehydrated}/domains"
    else
        echo "${srv_name}" > "${cDehydrated}/domains"
    fi

    echo "
KEY_ALGO=rsa
WELLKNOWN=\"${letsKnown}\"
CHALLENGETYPE=\"http-01\"
CONTACT_EMAIL=${srv_mail}
CERTDIR=\"${cDehydrated}/certs\"
LOCKFILE=\"${cDehydrated}/lock\"
ACCOUNTDIR=\"${cDehydrated}/accounts\"
CHAINCACHE=\"${cDehydrated}/chains\"
DOMAINS_TXT=\"${cDehydrated}/domains\"
ALPNCERTDIR=\"${cDehydrated}/alpn-certs\"
    " > "${cDehydrated}/conf"

    dehydrated --register --accept-terms --config "${cDehydrated}/conf"
    dehydrated -c --config /etc/letsencrypt/conf
    return_check $? "Probléme avec la génération du certif lets encrypt"

    (crontab -l 2>/dev/null; echo "@weekly dehydrated -c --config ${cDehydrated}/conf") | crontab -
}

function nginx_full()
{
    echo "
    server{
        listen 443;
        ssl on;
        server_name ${srv_name} ${srv_alias};
        ssl_certificate ${cDehydrated}/certs/${srv_name}/fullchain.pem;
        ssl_certificate_key ${cDehydrated}/certs/${srv_name}/privkey.pem;
        location / {
            proxy_pass http://127.0.0.1:${giteaPort};
            proxy_set_header Host             \$host;
            proxy_set_header X-Real-IP        \$remote_addr;
            proxy_set_header X-Forwarded-For  \$proxy_add_x_forwarded_for;
            client_max_body_size 1024M;
        }
    }" >> "${fnginx}/sites-available/gitea"

    systemctl restart nginx
    return_check $? "Start NGINX c'est mal passé, voir \"nginx -t\" et son log"
}

function gitea_app()
{
    curl "${uGitea}" -o "${bGitea}"
    chmod +x "${bGitea}"

    if [[ -z $(type -p adduser) ]]; then
        useradd -r -s /bin/bash -U -m ${giteaUser}
        usermod -c "Gitea" ${giteaUser}
    else
        adduser --system --shell /bin/bash --group -disabled-password \
            --home /home/${giteaUser} --disabled-login --gecos "Gitea" ${giteaUser}
    fi

    return_check $? "La creation de l'utilisateur c'est mal passé ..."
    mkdir -p "${cGitea}"
    
    echo "
[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target
After=memcached.service redis.service

[Service]
RestartSec=2s
Type=simple
User=${giteaUser}
Group=${giteaUser}
WorkingDirectory=/home/${giteaUser}/
ExecStart=${bGitea} web -c "${cGitea}/app.ini"
Restart=always
Environment=USER=${giteaUser} HOME=/home/${giteaUser} GITEA_WORK_DIR=/home/${giteaUser}

[Install]
WantedBy=multi-user.target
    " > ${sGitea}

    echo "
APP_NAME = ${aGiteaName}
RUN_USER = ${giteaUser}
RUN_MODE = prod

[security]
INTERNAL_TOKEN = $(gitea generate secret INTERNAL_TOKEN)
INSTALL_LOCK   = true
SECRET_KEY     = $(gitea generate secret SECRET_KEY)

[database]
DB_TYPE  = sqlite3
HOST     = 127.0.0.1:3306
NAME     = ${giteaUser}
USER     = ${giteaUser}
PASSWD   = 
SSL_MODE = disable
PATH     = /home/${giteaUser}/data/gitea.db

[repository]
ROOT               = /home/${giteaUser}/gitea-repositories
DEFAULT_PRIVATE    = private
PREFERRED_LICENSES = BSD-Protection

[markdown]
ENABLE_HARD_LINE_BREAK = true

[server]
SSH_DOMAIN       = ${srv_name}
DOMAIN           = ${srv_name}
HTTP_PORT        = 3000
ROOT_URL         = https://${srv_name}/
DISABLE_SSH      = false
SSH_PORT         = 22
LFS_START_SERVER = true
LFS_CONTENT_PATH = /home/${giteaUser}/data/lfs
LFS_JWT_SECRET   = $(gitea generate secret LFS_JWT_SECRET)
OFFLINE_MODE     = false

[mailer]
ENABLED       = false
DISABLE_HELO  = false
HOST          = mail.net:465
FROM          = Gitea Bot <giteabot@${srv_name}>
USER          = null@${srv_name}
PASSWD        = \`PassWord\`

[service]
REGISTER_EMAIL_CONFIRM            = false
ENABLE_NOTIFY_MAIL                = true
DISABLE_REGISTRATION              = false
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = false
REQUIRE_SIGNIN_VIEW               = false
DEFAULT_KEEP_EMAIL_PRIVATE        = false
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply.${srv_name}

[picture]
DISABLE_GRAVATAR        = false
ENABLE_FEDERATED_AVATAR = true

[openid]
ENABLE_OPENID_SIGNIN = false
ENABLE_OPENID_SIGNUP = false

[session]
PROVIDER = file

[log]
MODE      = file
LEVEL     = Info
ROOT_PATH = /home/${giteaUser}/log

[attachment]
MAX_SIZE      = 8
ALLOWED_TYPES = */*

[git]
MAX_GIT_DIFF_LINES = 500

[markup.asciidoc]
ENABLED = false
FILE_EXTENSIONS = .adoc,.asciidoci
RENDER_COMMAND = \"asciidoc --out-file=- -\"
IS_INPUT_FILE = false
    " > "${cGitea}/app.ini"

    systemctl enable gitea
    systemctl start gitea
    return_check $? "Impossible de start gitea, voir debug/log"

    su -c "gitea admin create-user --admin --name root --password \"${aGiteaPwd}\" --email \"${srv_mail}\" -c \"${cGitea}/app.ini\"" gitea
    echo "INFO: Le compte gitea admin a pour nom \"root\" sont mot de passe est \"${aGiteaPwd}\""
}

# MAIN
# Test root
if [[ $EUID -ne 0 ]]; then
    err "${0##*/} must be run as root"
fi

# Test config
require

fullVar="aGiteaName srv_core srv_mail srv_name"
for conf in $fullVar; do
    if [[ -z ${!conf} ]]; then
        err "Variables de conf \"${conf}\" vide"
    fi
done

if [[ -z ${srv_ip} ]]; then
    srv_ip=$(dig +short myip.opendns.com @resolver1.opendns.com)
fi

if [[ -z ${aGiteaPwd} ]]; then
    aGiteaPwd=$(openssl rand -base64 8)
fi

# Let's "go"
nginx_base
dehydrated_app
nginx_full
gitea_app