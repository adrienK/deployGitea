|![gitea logo](https://docs.gitea.io/images/favicon.png)|
| -------- |
# DeployGitea
>*Script Bash 
>pour déployer un serveur GiTea sous Gnu/Linux*

## :page_with_curl: **DESCRIPTION**
**DeployGitea.sh** est un simple script Bash 
Il permet de deployer rapidement un serveur **GiTea** avec certif SSL
> **Il a était utilisé pour déployer ce serveur ci (git.iglou.eu)**

Il ne fonctionne que sous Gnu/Linux, mais sera peut étre adapté pour OpenBSD :heart:

## :rocket: **HOW TO**
### Pour l'utilisez
téléchargez le script, éditez la configuration et executez

    # curl "https://git.iglou.eu/adrien/deployGitea/raw/branch/master/deployGitea.sh" -o deployGitea.sh
    # vim deployGitea.sh
    # chmod +x deployGitea.sh
    # ./deployGitea.sh

### La configuration
Si une conf obligatoire est laissé vide, le script renvoi une erreur
*En gras les conf obligatoire*
* **aGiteaName**, *le nom de l'application (ex: Mon super git)*
* aGiteaPwd, *le mot de passe du compte root, si la valeur est vide un mot de passe est généré avec "openssl rand"*
* **srv_core**, *spécifier l'architecture cpu, pour dl le binaire GiTea (dispo: 386, amd64, arm-5/6/7/64, mips, mips64, mipsle)*
* **srv_mail**, email de l'administrateur pour certif let's encrypt
* **srv_name**, nom de domaine principale (ex: git.iglou.eu)
* srv_alias, alias du nom de domaine mettre un espace entre chaque alias (ex: b.iglou.eu c.iglou.eu)
* srv_ip, ip du serveur peut étre laissé vide

## :construction: **INSTALLATION**
### Testé sous
**Distributions:** Debian, Ubuntu et ArchLinux
**Architecture:** amd64 et arm7

### Dépendances requises
**git go nginx curl bash crontab dig**

**dig** est dans le package: dnsutils(debian/ubuntu) ou bind-tools(ArchLinux)
**go** est dans le package: golang(debian/ubuntu) ou go(ArchLinux)
**crontab** est dans le package: cronie

*bash git go nginx, Sont requis par Gitea
curl et dig, Sont requis pour le script
crontab Est requi pour le renouvellement let's encrypt*

## :interrobang: **FAQ**
### Personalisation de GiTea
Pour personaliser GiTea, il faut cloner le fichier que l'on veut personaliser dans le dossier "custom" en reproduisant son arborescence (ex: https://git.iglou.eu/Iglou.eu/gitea_custom)

### Update GiTea
Pour mettre a jours GiTea, il faut arréter le service gitea, télécharger le nouveau binaire pour remplacer l'ancien et relancer le service.

*Remplacer ${srv_core} par votre architecture et ${bGitea} par le chemain vers le binaire*
*Par default ${bGitea} = "/usr/bin/gitea"*

    # systemctl stop gitea
    # curl "https://dl.gitea.io/gitea/master/gitea-master-linux-${srv_core}" -o ${bGitea}
    # systemctl start gitea

### Variables avancé
Dans le script il y a des variables avancé, qui permetent de personaliser un peut le deployement 
https://git.iglou.eu/adrien/deployGitea/src/branch/master/deployGitea.sh#L12

Comme, changer le port gitea, l'utilisateur, l'emplacement des binaires/scripts ...
Les valeurs par default sont:

```
require="git go nginx curl bash crontab dig"

fnginx="/etc/nginx"

letsKnown="/var/www/letsencrypt"
bDehydrated="/usr/bin/dehydrated"
cDehydrated="/etc/letsencrypt"
uDehydrated="https://raw.githubusercontent.com/lukas2511/dehydrated/master/dehydrated"

giteaUser="gitea"
giteaPort="3000"
bGitea="/usr/bin/gitea"
cGitea="/etc/gitea"
sGitea="/etc/systemd/system/gitea.service"
uGitea="https://dl.gitea.io/gitea/master/gitea-master-linux-${srv_core}"
```